var Client = require('node-rest-client').Client;
var readline =require('readline');
var key ='a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5';
var url1 = 'http://api.wordnik.com:80/v4/';
var client =new Client();
var syn_ant,play_word,guess_word;
var rl = readline.createInterface({
    input:process.stdin,
    output:process.stdout
  });
function def(){
  var type='word.json/';
  var url2='/definitions?limit=200&includeRelated=true&useCanonical=false&includeTags=false&api_key=';
  var word = process.argv[3];
  console.log("definitions");
  client.get(url1+type+word+url2+key,function(data,response){
    if(data.length<1){
      console.log("No such word found");
    }else{
    for(var i=0;i<data.length;i++){
      console.log(i+1+") "+data[i].text);
    }
  }
  //console.log(response,"xyz");
  });
};
function syn(){
  var type ='word.json/';
  var url2 ='/relatedWords?useCanonical=false&relationshipTypes=synonym&limitPerRelationshipType=100&api_key=';
  var word = process.argv[3];
  console.log("synonyms");
  client.get(url1+type+word+url2+key,function(data,response){
    //console.log(data.length);
    if(data.length==0){
      console.log("Word doesn't have Synonyms");
    }else{
     for(var i=0;i<data[0].words.length;i++){
      console.log(data[0].words[i]);
     }
   }
  });
};
function ant(){
  var type = 'word.json/';
  var url2 ='/relatedWords?useCanonical=false&relationshipTypes=antonym&limitPerRelationshipType=100&api_key=';
  var word = process.argv[3];
  console.log("antonyms");
  //console.log("This is your word: ",word);
  client.get(url1+type+word+url2+key,function(data,response){
    if(data.length==0){
      console.log("Word doesn't have Antonyms");
    }else{
     for(var i=0;i<data[0].words.length;i++){
      console.log(data[0].words[i]);
     }
   }
  });
};
 function ex(){
  var type = 'word.json/';
  var url2 = '/examples?includeDuplicates=false&useCanonical=false&skip=0&limit=5&api_key=';
  var word = process.argv[3];
  console.log("examples");
  //console.log("This is your word: ",word);
  client.get(url1+type+word+url2+key,function(data,response){
    //console.log(data.examples);
    if(!data.examples){
      console.log("No such word found");
    }else{
     for(var i=0;i<data.examples.length;i++){
      console.log(i+1+") "+data.examples[i].text);
     }
   }
  });
};
function day(){
var type = 'words.json/wordOfTheDay?date=';
var url2='&api_key=';
var date=new Date();
var year=date.getFullYear();
var month=date.getMonth()+1;
var dt=date.getDate();
if (dt<10){
  dt ='0'+dt;
}
if(month<10){
  month = '0'+month;
}
var fdate=year+'-'+month+'-'+dt;
console.log(fdate);
var ref;
client.get(url1+type+fdate+url2+key,function(data,response){
  ref= data;
  console.log(ref.word);
  console.log("examples");
  for(var i=0;i<data.examples.length;i++){
  console.log(data.examples[i].text);
  }
  console.log("definitions");
  for(var i=0;i<data.definitions.length;i++){
    console.log(data.definitions[i].text);
  }
});
syn_ant = setTimeout(function(){
var type1 ='word.json/';
var nurl2 ='/relatedWords?useCanonical=false&limitPerRelationshipType=10&api_key=';
var word = ref.word;
client.get(url1+type1+word+nurl2+key,function(data,response){
  //console.log(data);
  if (data.length<1) {
    console.log("Data not found");
  }
  else{
  for(var i=0;i<data.length;i++){
      if(data[i].relationshipType==='antonym'){
        console.log("Antonyms");
        for (var j = 0; j< data[i].words.length; j++) {
        console.log(data[i].words[j]);
        }
      }
      if(data[i].relationshipType==='synonym'){
        console.log("Synonyms");
        for (var j = 0; j< data[i].words.length; j++) {
        console.log(data[i].words[j]);
        }
      }
      else{
        console.log("No Synonyms or Antonyms");
      }
    }
  }
});
},1000);
};
function play(){
  console.log("Welcome to the game");
  console.log("Here are your options:");
  console.log("To guess the word, enter 1");
  console.log("For a hint enter 2");
  console.log("To quit enter 3");
  console.log("Here we go!");
 rl.on('line',function(line) {
    switch(line.trim()){
      case '1':
      check();
      break;
      case '2':
      hint();
      break;
      case '3':
      quit();
      break;
    }
  }).on('close',function(){
    console.log('Have a great day!');
  process.exit(0);
});
  var type = 'words.json/randomWord?hasDictionaryDef=false&minCorpusCount=0&maxCorpusCount=-1&minDictionaryCount=1&maxDictionaryCount=-1&minLength=5&maxLength=-1';
  var url2='&api_key=';
  client.get(url1+type+url2+key,function(data,response){
    play_word=data.word;
    var url2='/definitions?limit=200&includeRelated=true&useCanonical=false&includeTags=false&api_key=';
    var type='word.json/';
    console.log("Defination(s) of the word");
    var getdata=setTimeout(function(){
      client.get(url1+type+data.word+url2+key,function(data,response){
      for(var i=0;i<data.length;i++){
        console.log(i+1+") "+data[i].text);
      }
    });
  },1000);
});
};
function check(){

rl.question('Your guess:',function(guess){
    guess_word=guess;
  console.log(guess);
  if(guess.toUpperCase()==play_word.toUpperCase()) {
    console.log("Congratulations! You've done it");
    rl.close();
    console.log('Have a great day!');
    process.exit(0);
  }
  else{
    check2();
  }
});
};
function check2(){
  var type ='word.json/';
  var url2 ='/relatedWords?useCanonical=false&limitPerRelationshipType=10&api_key=';
  client.get(url1+type+play_word+url2+key,function(data,response){
    if(data.length>0){
      var count;
      if(guess_word.toUpperCase()==data[data.length-1].words[0].toUpperCase()){
          console.log("That's the hint. Try another similar word");
          check();
        }
        else{
    for(var i=0;i<data.length;i++){
        if(data[i].relationshipType=='antonym'||data[i].relationshipType=='rhyme'){
          //console.log("Can't help you out, Try again!");
        }
        else{
          data[i].words.forEach(function(Word) {
           if(Word.toUpperCase()==guess_word.toUpperCase()){
            console.log("you have done it. The word is: ",play_word);
            //rl.close();
            process.exit(0);
          }
          else{
              count=i;
              //console.log(count);
          }
          });
          if(count===data.length-1){
            console.log("Your guess doesn't match with our data, Sorry!");
            check();
          }

        }
      }
    }
    }
      else{
        console.log("Try Again");
        check();
      }
});
};
function quit(){
console.log("Well, the word is:",play_word);
console.log("Thanks for playing");
process.exit(0);
};
function hint(){
  var type ='word.json/';
  var url2 ='/relatedWords?useCanonical=false&limitPerRelationshipType=10&api_key=';
  console.log("Here is your hint:");
  client.get(url1+type+play_word+url2+key,function(data,response){
    if(data.length==0||!data[0].words){
      console.log("sorry, we can't help you!");
      check();
    }else{
    //console.log(data);
    console.log(data[data.length-1].relationshipType,data[data.length-1].words[0]);
    check();
  }
  });
};
switch(process.argv[2]){
  case "def":
  def();
  break;
  case "syn":
  syn();
  break;
  case "ant":
  ant();
  break;
  case "ex":
  ex();
  break;
  case "day":
  day();
  break;
  case "play":
  play();
  break;
  case "dict":
  def();
  setTimeout(syn,1000);
  setTimeout(ant,2000);
  setTimeout(ex,3000);
  break;
}
